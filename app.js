const appRoot = document.getElementById('app-root'); // add all your elements into this element
const inputSearch = document.createElement("input");
let currCountries
inputSearch.addEventListener('input', () => {
    const inputValue = inputSearch.value
    if (regionsSelector.classList.contains('active')) {
        const regionOption = regionsSelector.options[regionsSelector.selectedIndex].text
        currCountries = externalService.getCountryListByRegion(regionOption)
    } else if (languageSelector.classList.contains('active')) {
        const languageOption = languageSelector.options[languageSelector.selectedIndex].text
        currCountries = externalService.getCountryListByLanguage(languageOption)
    }
    currCountries.length > 0 ? currCountries : currCountries = externalService.getAllCountries()
    validationCountries = currCountries.filter((country) => country.name.startsWith(inputValue))
    deletePrevBody()
    const tableBody = drawTable(validationCountries)
    currCountries = validationCountries
    table.append(tableBody)
})
appRoot.appendChild(inputSearch)

const selectLocation = document.createElement('select')

let currentIndexChoice = 0
const selectAction = document.createElement("select")
selectAction.addEventListener('change', () => {
    currentIndexChoice = selectAction.selectedIndex
    let countries = externalService.getAllCountries()
    switch(currentIndexChoice) {
        case 1:
            regionsSelector.classList.contains('active') ? regionsSelector.classList.remove('active') : ''
            languageSelector.classList.add('active'); break;
        case 2:
            languageSelector.classList.contains('active') ? languageSelector.classList.remove('active') : ''
            regionsSelector.classList.add('active'); break;
        default:
            regionsSelector.classList.contains('active') ? regionsSelector.classList.remove('active') : ''
            languageSelector.classList.contains('active') ? languageSelector.classList.remove('active') : ''
            const inputValue = inputSearch.value
            inputValue.length > 0 ? countries = countries.filter((country) => country.name.startsWith(inputValue)) : countries
            const tableBody = drawTable(countries)
            currCountries = countries
            const currBody = table.querySelector('tbody')
            currBody ? table.removeChild(currBody) : ''
            table.append(tableBody)
    }
})

const defaultChoice = document.createElement("option")
defaultChoice.setAttribute('value', '')
defaultChoice.innerText = 'Выберите параметр'

const langChoice = document.createElement("option")
langChoice.setAttribute('value', 'language')
langChoice.innerText = 'Поиск по языку'

const locationChoice = document.createElement("option")
locationChoice.setAttribute('value', 'location')
locationChoice.innerText = 'Поиск по региону'

selectAction.appendChild(defaultChoice)
selectAction.appendChild(langChoice)
selectAction.appendChild(locationChoice)
appRoot.appendChild(selectAction)

const languageSelector = createLanguagesSelect()
languageSelector.addEventListener('change', () => {
    const currLanguage = languageSelector.options[languageSelector.selectedIndex].text
    currCountries = externalService.getCountryListByLanguage(currLanguage)
    const inputValue = inputSearch.value
    const validationCountries = currCountries.filter((country) => country.name.startsWith(inputValue))
    deletePrevBody()
    const tableBody = drawTable(validationCountries)
    currCountries = validationCountries
    table.append(tableBody)
})
appRoot.appendChild(languageSelector)

const regionsSelector = createRegionsSelect()
regionsSelector.addEventListener('change', () => {
    const currRegion = regionsSelector.options[regionsSelector.selectedIndex].text
    currCountries = externalService.getCountryListByRegion(currRegion)
    const inputValue = inputSearch.value
    const validationCountries = currCountries.filter((country) => country.name.startsWith(inputValue))
    deletePrevBody()
    currCountries = validationCountries
    const tableBody = drawTable(validationCountries)
    table.append(tableBody)
})
appRoot.appendChild(regionsSelector)

let sortedByAlphabet = false
let sortedBySquare = false

const alphabetFilter = createAlphabetFilter()
const squareFilter = createSquareFilter()

alphabetFilter.addEventListener('click', () => {
    let sortedCountries
    if (sortedByAlphabet) {
        sortedByAlphabet = !sortedByAlphabet
        alphabetFilter.classList.toggle('active-filter')
        sortedCountries = (currCountries.sort((a, b) => {
            const nameA = a.name.toUpperCase()
            const nameB = b.name.toUpperCase()
            if (nameA < nameB) {
                return -1;
              }
              if (nameA > nameB) {
                return 1;
              }
              return 0
        }))
    } else {
        sortedByAlphabet = !sortedByAlphabet
        alphabetFilter.classList.toggle('active-filter')
        sortedCountries = (currCountries.sort((a, b) => {
            const nameA = a.name.toUpperCase()
            const nameB = b.name.toUpperCase()
            if (nameA > nameB) {
                return -1;
              }
              if (nameA < nameB) {
                return 1;
              }
              return 0
        }))
    }
    const tableBody = drawTable(sortedCountries)
    currCountries = sortedCountries
    const currBody = table.querySelector('tbody')
    currBody ? table.removeChild(currBody) : ''
    table.append(tableBody)
})

squareFilter.addEventListener('click', () => {
    let sortedCountries
    if (sortedBySquare) {
        sortedBySquare = !sortedBySquare
        squareFilter.classList.toggle('active-filter')
        sortedCountries = (currCountries.sort((a, b) => a.area - b.area))
    } else {
        sortedBySquare = !sortedBySquare
        squareFilter.classList.toggle('active-filter')
        sortedCountries = (currCountries.sort((a, b) => b.area - a.area))
    }
    const tableBody = drawTable(sortedCountries)
    currCountries = sortedCountries
    const currBody = table.querySelector('tbody')
    currBody ? table.removeChild(currBody) : ''
    table.append(tableBody)
})
const table = createTable(alphabetFilter, squareFilter)

const tableBody = drawTable(externalService.getAllCountries())
currCountries = externalService.getAllCountries()
table.appendChild(tableBody)
appRoot.appendChild(table)

function createTable(aFilter, sFilter) {
    const table = document.createElement('table')
    const headers = document.createElement('thead')
    const headersRow = document.createElement('tr')
    const keys = Object.keys(externalService.getAllCountries()[0])
    keys.forEach((countryProperty, index) => {
        const newColumnHead = document.createElement('th')
        if (index === 0) {
            newColumnHead.innerText = countryProperty
            newColumnHead.appendChild(aFilter)
        } else if (index === 3){
            newColumnHead.innerText = countryProperty
            newColumnHead.appendChild(sFilter)
        } else {
            newColumnHead.innerText = countryProperty
        }
        headersRow.appendChild(newColumnHead)
    })
    headers.appendChild(headersRow)
    table.append(headers)
    return table
}

function drawTable(countriesData) {
    const tableBody = document.createElement('tbody')
    countriesData.forEach((country, index) => {
        const tableRow = document.createElement('tr')
        Object.values(country).forEach((countryProperty, index) => {
            const tableCol = document.createElement('td')
            if (index % 5 === 0 && index != 0) {
                tableCol.innerText = Object.values(countryProperty)
            } else {
                tableCol.innerText = countryProperty
                tableRow.appendChild(tableCol)
            }
            tableRow.appendChild(tableCol)
        })
        tableBody.appendChild(tableRow)
    })
    return tableBody
}

function createLanguagesSelect() {
    const selectLanguage = document.createElement('select')
    selectLanguage.classList.add('select-language') 
    const defaultOption = document.createElement('option')
    defaultOption.setAttribute('value', '')
    defaultOption.innerText = 'Выберите язык'
    selectLanguage.appendChild(defaultOption)
    const languages = externalService.getLanguagesList()

    languages.forEach((lang, index) => {
        const langOption = document.createElement('option')
        langOption.setAttribute('value', lang)
        langOption.innerText = lang
        selectLanguage.appendChild(langOption)
    })
    return selectLanguage
}

function createRegionsSelect() {
    const selectRegion = document.createElement('select')
    selectRegion.classList.add('select-region')
    const defaultOption = document.createElement('option')
    defaultOption.setAttribute('value', '')
    defaultOption.innerText = 'Выберите регион'
    selectRegion.appendChild(defaultOption)
    const regions = externalService.getRegionsList()

    regions.forEach((region, index) => {
        const regionOption = document.createElement('option')
        regionOption.setAttribute('value', region)
        regionOption.innerText = region
        selectRegion.appendChild(regionOption)
    })
    return selectRegion
}

function filterByRegion() {
    const selectRegion = document.createElement('select')
    selectRegion.classList.add('select-region')
    const regions = externalService.getRegionsList()
    regions.forEach((region, index) => {
        const regionOption = document.createElement('option')
        regionOption.innerText = region
        selectRegion.appendChild(regionOption)
    })
    return selectRegion
}

function deletePrevBody() {
    const currBody = table.querySelector('tbody')
    currBody ? table.removeChild(currBody) : ''
}

function createAlphabetFilter() {
    const filterIcon = document.createElement('span')
    filterIcon.classList.add('alphabet-filter')
    return filterIcon
}

function createSquareFilter() {
    const filterIcon = document.createElement('span')
    filterIcon.classList.add('square-filter')
    return filterIcon
}
